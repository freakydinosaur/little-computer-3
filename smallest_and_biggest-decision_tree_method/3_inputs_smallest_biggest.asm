;------------------------------------------------------------------------------;
;	-REGISTER ASSIGNMENTS-							;
;										;
;	R0 = Read/Write								;
;	R1 = Temp1								;
;	R2 = Temp2								;
;	R3 = Condition Result							;
;	R4 = Store value of input 1						;
;	R5 = Store value of input 2						;
;	R6 = Store value of input 3						;
;	R7 = Reserved for program						;
;------------------------------------------------------------------------------;
;///////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;

.ORIG x3000			;start writing at location 0x3000 of the RAM

;*******************************DEBUG INPUT L00P*******************************;
L00P					;Label
	LEA  R0, Debug		;load address of label Debug to R0
	PUTS			;Print out string in label NewLine
	LEA  R0, NewLine	;load address of label NewLine to R0
	PUTS			;Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

;------------------------------------------------------------------------------;
; This clears the registers at the beginning of the code, you can reload the
; program without reinitalize the simulator and clearing the registers

	AND  R0, R0, #0		;Clear R0
	AND  R1, R1, #0		;Clear R1
	AND  R2, R2, #0		;Clear R2
	AND  R3, R3, #0		;Clear R3
	AND  R4, R4, #0		;Clear R4
	AND  R5, R5, #0		;Clear R5
	AND  R6, R6, #0		;Clear R6
	AND  R7, R7, #0		;Clear R7

;------------------------------------------------------------------------------;
STUDENT				; Label
	LEA R0, Student		; Load address of StudentID to R0
	PUTS			; Print out StudentID

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////////;

;//////////////////////////////////-Input 1-\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;

INPUT1				; Label
	LEA R0, Number1		; Load address of Number1 to R0
	PUTS			; Print out Number1

	GETC			; Gets charicter saved into R0
	OUT			; Prints out the number entered

	ADD R4, R0, #0		; Takes R0 saves number into a R4

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-Input 1-///////////////////////////////////;

;//////////////////////////////////-Input 2-\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;

INPUT2				; Label
	LEA R0, Number2		; Load address of Number2 to R0
	PUTS			; Print out Number2

	GETC			; Gets charicter saved into R0
	OUT			; Prints out the number entered

	ADD R5, R0, #0		; Takes R0 saves number into a R5

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-Input 2-///////////////////////////////////;

;//////////////////////////////////-Input 3-\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;

INPUT3				; Label
	LEA R0, Number3		; Load address of Number3 to R0
	PUTS			; Print out Number3

	GETC			; Gets charicter saved into R0
	OUT			; Prints out the number entered

	ADD R6, R0, #0		; Takes R0 saves number into a R6

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-Input 3-///////////////////////////////////;

;==============================================================================;
;==============================================================================;
;==============================================================================;

;/////////////////////////////-COMPUTE COMPARSION-\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;
;////////////////////////////////// (a < b) \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;

;------------------------------(R4 < R5)=(a < b)-------------------------------;

BEGIN				; Label

;------------------------------------------------------------------------------;
; This clears registers R0, R1, R2, R3 before computation is made to avoid the
; values left over being made into a new computation.

	AND  R0, R0, #0		; Clear R0
	AND  R1, R1, #0		; Clear R1
	AND  R2, R2, #0		; Clear R2
	AND  R3, R3, #0		; Clear R3

;------------------------------------------------------------------------------;

	ADD  R1, R4, #0		; Load Input 1 (R4) to R1
	ADD  R2, R5, #0		; Load Input 2 (R5) to R2

	NOT  R3, R2		; Turn second number to negative form
	ADD  R3, R3, #1		; Then store in R3, to check if (R4 - R5) > 0
	ADD  R3, R1, R3		; Store the difference to R3 = (R4-R5)
		BRzp ONEE	; If (R4 < R5) = True. Jump to ONEE else go to
				;next line which is label AAAA
;
; Watch REGISTER 3 if the result is a negative number the statement is true if
; the result is a postive number the statement is false so jump to label FALSE

;==============================================================================;
;==============================================================================;
;==============================================================================;

;==============================================================================;
;-RIGHT BRANCH---------(R4 < R5) WAS TRUE SO GO TO-AAAA------------------------;
;---------------------LABEL AAAA---NOW COMPUTE (R4 < R6)-----------------------;

AAAA				;Label x

;------------------------------------------------------------------------------;
; This clears registers R0, R1, R2, R3 before computation is made to avoid the
; values left over being made into a new computation.

	AND  R0, R0, #0		; Clear R0 - Read/Write
	AND  R1, R1, #0		; Clear R1 - Temp1
	AND  R2, R2, #0		; Clear R2 - Temp2
	AND  R3, R3, #0		; Clear R3 - Condition Result

;------------------------------------------------------------------------------;

	ADD  R1, R4, #0     	; Load Input 3 (R4) to R1
	ADD  R2, R6, #0     	; Load Input 2 (R6) to R2

	NOT  R3, R2		; Turn second number to negative form
	ADD  R3, R3, #1		; Then store in R3, to check if (R4 - R6) > 0
	ADD  R3, R1, R3		; Store the difference to R3 = (R4-R6)
		BRzp EEEE	; If (R4 < R6) = True. Jump to EEEE else go to
				; next line which is label BBBB

; Watch REGISTER 3 if the result is a negative number the statement is true if
; the result is a postive number the statement is false so jump to label FALSE

;==============================================================================;
;-RIGHT BRANCH---------(R4 < R6) WAS TRUE SO GO TO-BBBB------------------------;
;----------------------LABEL BBBB---NOW COMPUTE (R6 < R5)----------------------;

BBBB				; Label x

;------------------------------------------------------------------------------;
; This clears registers R0, R1, R2, R3 before computation is made to avoid the
; values left over being made into a new computation.

	AND  R0, R0, #0		; Clear R0 - Read/Write
	AND  R1, R1, #0		; Clear R1 - Temp1
	AND  R2, R2, #0		; Clear R2 - Temp2
	AND  R3, R3, #0		; Clear R3 - Condition Result
;------------------------------------------------------------------------------;

	ADD  R1, R6, #0     	; Load Input 3 (R6) to R1
	ADD  R2, R5, #0     	; Load Input 2 (R5) to R2

	NOT  R3, R2		; Turn second number to negative form
	ADD  R3, R3, #1		; Then store in R3, to check if (R6 - R5) > 0
	ADD  R3, R1, R3		; Store the difference to R3 = (R6-R5)
		BRzp DDDD	; If (R6 < R5) = True. Jump to DDDD else go to
				; next line which is label CCCC
;
; Watch REGISTER 3 if the result is a negative number the statement is true if
; the result is a postive number the statement is false so jump to label FALSE

;==============================================================================;
;;-RIGHT BRANCH--------(R6 < R5) WAS TRUE SO PRINT RESULT----------------------;
;------------------------LABEL CCCC----NOW PRINT RESULT------------------------;
; SMALL = R4
; LARGE = R5

CCCC				; Label x

;-------------------------Print the LargesT Number-----------------------------;

	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS			; Print out string in label SmallesT

	ADD R0, R4, #0		; Take R4 and copy into R0
	OUT                 	; Output character in R0 to the console

;-------------------------Print the LargesT Number-----------------------------;

	LEA R0, LargesT		; Load address of label LargesT to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R5, #0		; Take R5 and copy into R0
	OUT			; Output character in R0 to the console

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

		BR FINISH	; Jump to the label FINISH / end of code

;==============================================================================;
;-RIGHT BRANCH---------(R6 < R5) WAS FALSE SO PRINT RESULT---------------------;
;------------------------LABEL DDDD----NOW PRINT RESULT------------------------;
; SMALL = R4
; LARGE = R6
;

DDDD				; Label x

;-------------------------Print the LargesT Number-----------------------------;
	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS			; Print out string in label SmallesT

	ADD R0, R4, #0		; Take R4 and copy into R0
	OUT                 	; Output character in R0 to the console

;-------------------------Print the LargesT Number-----------------------------;
	LEA R0, LargesT		; Load address of label LargesT to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R6, #0		; Take R6 and copy into R1
	OUT			; Output character in R0 to the console

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

		BR FINISH	; Jump to the label FINISH / end of code

;==============================================================================;
;-RIGHT BRANCH---------(R6 < R5) WAS FALSE SO PRINT RESULT---------------------;
;------------------------LABEL EEEE----NOW PRINT RESULT------------------------;
; SMALL = R6
; LARGE = R5

EEEE				; Label x

;-------------------------Print the LargesT Number-----------------------------;
	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS			; Print out string in label SmallesT

	ADD R0, R6, #0		; Take R6 and copy into R0
	OUT                 	; Output character in R0 to the console display
;-------------------------Print the LargesT Number-----------------------------;
	LEA R0, LargesT		; Load address of StudentID to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R5, #0		; Take R5 and copy into R0
	OUT			; Output character in R0 to the console display

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS 			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

		BR FINISH	; Jump to the label FINISH / end of code

;==============================================================================;

;==============================================================================;
;==============================================================================;
;==============================================================================;

;==============================================================================;
;-LEFT BRANCH----------(R4 < R5) WAS FALSE SO GO TO ONEE-----------------------;
;---------------------LABEL ONEE---NOW COMPUTE (R4 < R6)-----------------------;

ONEE 				; Label x

;------------------------------------------------------------------------------;
; This clears registers R0, R1, R2, R3 before computation is made to avoid the
; values left over being made into a new computation.

	AND  R0, R0, #0		; Clear R0 - Read/Write
	AND  R1, R1, #0		; Clear R1 - Temp1
	AND  R2, R2, #0		; Clear R2 - Temp2
	AND  R3, R3, #0		; Clear R3 - Condition Result

;------------------------------------------------------------------------------;

	ADD  R1, R4, #0 	; Load Input 1 (R4) to R1
	ADD  R2, R6, #0 	; Load Input 3 (R6) to R2
	NOT  R3, R2 		; Turn second number to negative form
	ADD  R3, R3, #1		; Then store in R3, to check if (R4 - R6) > 0
	ADD  R3, R1, R3		; Store the difference to R3 = (R4 - R6)
		BRzp TWOO 	; If (R4 < R6) = False. Jump to label TWOO
		BR FIVE 	; If (R4 < R6) = True. Jump to label FIVE
;
; Watch REGISTER 3 if the result is a negative number the statement is true if
; the result is a postive number the statement is false so jump to label FALSE
;==============================================================================;
;-LEFT BRANCH----------(R4 < R6) WAS FALSE SO GO TO TWOO-----------------------;
;----------------------LABEL TWOO---NOW COMPUTE (R6 < R5)----------------------;

TWOO 				; Label x

;------------------------------------------------------------------------------;
; This clears registers R0, R1, R2, R3 before computation is made to avoid the
; values left over being made into a new computation.

	AND  R0, R0, #0		; Clear R0 - Read/Write
	AND  R1, R1, #0		; Clear R1 - Temp1
	AND  R2, R2, #0		; Clear R2 - Temp2
	AND  R3, R3, #0		; Clear R3 - Condition Result

;------------------------------------------------------------------------------;

	ADD  R1, R6, #0 	; Load Input 3 (R6) to R1
	ADD  R2, R5, #0 	; Load Input 2 (R5) to R2

	NOT  R3, R2 		; Turn second number to negative form
	ADD  R3, R3, #1		; Then store in R3, to check if (R6 - R5) > 0
	ADD  R3, R1, R3		; Store the difference to R3 = (R6 - R5)
		BRzp THREE 	; If (R6 < R5) = False. Jump to label THREE
		BR FOUR 	; If (R6 < R5) = True. Jump to label FOUR
;
; Watch REGISTER 3 if the result is a negative number the statement is true if
; the result is a postive number the statement is false so jump to label FALSE
;==============================================================================;
;-LEFT BRANCH----------(R6 < R5) WAS TRUE SO PRINT RESULT----------------------;
;------------------------LABEL THREE----NOW PRINT RESULT-----------------------;
; SMALL = R5
; LARGE = R4

THREE				; Label x

;-------------------------Print the LargesT Number-----------------------------;
	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS				; Print out string in label SmallesT

	ADD R0, R5, #0		; Take R5 and copy into R0
	OUT                 ; Output character in R0 to the console

;-------------------------Print the LargesT Number-----------------------------;
	LEA R0, LargesT		; Load address of label LargesT to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R4, #0		; Take R4 and copy into R0
	OUT			; Output character in R0 to the console

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

		BR FINISH	; Jump to the label FINISH / end of code

;==============================================================================;
;-LEFT BRANCH----------(R6 < R5) WAS TRUE SO PRINT RESULT----------------------;
;------------------------LABEL FOUR----NOW PRINT RESULT------------------------;
; SMALL = R6
; LARGE = R4

FOUR				; Label x

;-------------------------Print the LargesT Number-----------------------------;
	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS			; Print out string in label SmallesT

	ADD R0, R6, #0		; Take R6 and copy into R0
	OUT                 ; Output character in R0 to the console

;-------------------------Print the LargesT Number-----------------------------;
	LEA R0, LargesT		; Load address of label LargesT to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R4, #0		; Take R4 and copy into R0
	OUT			; Output character in R0 to the console

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;

		BR FINISH	; Jump to the label FINISH / end of code

;==============================================================================;
;-LEFT BRANCH----------(R4 < R6) WAS TRUE SO PRINT RESULT----------------------;
;------------------------LABEL FIVE----NOW PRINT RESULT------------------------;
; SMALL = R5
; LARGE = R6

FIVE				; Label x

;-------------------------Print the LargesT Number-----------------------------;
	LEA  R0, SmallesT	; Load address of label SmallesT to R0
	PUTS			; Print out string in label SmallesT

	ADD R0, R5, #0		; Take R5 and copy into R0
	OUT			; Output character in R0 to the console display
;-------------------------Print the LargesT Number-----------------------------;
	LEA R0, LargesT		; Load address of StudentID to R0
	PUTS			; Print out string in label LargesT

	ADD R0, R6, #0		; Take R6 and copy into R0
	OUT			; Output character in R0 to the console display
;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS			; Print out string in label NewLine
;*******************************DEBUG INPUT L00P*******************************;
		BR FINISH	; Jump to the label FINISH / end of code

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////////;
;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-COMPUTE COMPARSION-/////////////////////////////;

;==============================================================================;
;==============================================================================;
;==============================================================================;

FINISH				; Label x. Finish is a Label for the end of code

;*******************************DEBUG INPUT L00P*******************************;
	LEA  R0, NewLine	; Load address of label NewLine to R0
	PUTS 			; Print out string in label NewLine
		BR L00P		; Jump to the label L00P at start of code
;*******************************DEBUG INPUT L00P*******************************;

HALT				; Print out halt and stop the program

;///////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\;
; Reserved memory space for variable assignments

Student		.STRINGZ "John Doe, ID number: 123456789"
Debug		.STRINGZ "------DEBUG INPUT L00P---------"
NewLine		.STRINGZ "\n"
Number1		.STRINGZ "\nPlease enter number 1? "
Number2		.STRINGZ "\nPlease enter number 2? "
Number3		.STRINGZ "\nPlease enter number 3? "
SmallesT	.STRINGZ "\nThe smallest number is: "
LargesT		.STRINGZ "\nThe largest number is: "

;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\///////////////////////////////////////;

.END				; Terminate program
