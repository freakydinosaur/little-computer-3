## Input 3 numbers > Comparsion > Output smallest and biggest number.

This program takes in three inputs in its ASCII value (ex. ASCII 9 = Decimal 57) and compares each number with the statement *a is less than b* in the decision tree. The final decision prints out the resulting smallest and largest values.
There are five conditions used to get to the final result.

There isn't a need convert the ASCII value to its real number to do the computation and back to print the value out to console.
The ASCII values are sequential anyway.

Run in debug and trace the program to see what is happening in the progam

Watch **Register 3** to see the result of the condition.

**Avoid copying line for line.**

**Rename labels, change comments, change register assignment, change indentation format, change cases, move blocks of code keeping JUMPS intact if you need to.**

| Logical | LC3 branch instruction | LC3 negated branch instruction |
| ------ | ------ | ------ |
| if (a < b) | BRn | BRzp |

In each branch the program just uses only the negated branch instruction BRzp for all decisions so you get the statement being reversed in the other branch. You can use the instruction BRn to have the statement consistent.

In the left branch, refer to the flowchart.

if the condition result is a **negative number** the statement is false

if the condition result is a **postive number** the statement is true then make the jump

In the right branch, its reversed.

if the condition result is a **negative number** the statement is true.

if the condition result is a **postive number** the statement is false then make the jump.



#### Debug Loop.
The Debug Loop is to test a combination of numbers without the need to reinitalize the simulator or reload the program.

Remove section of debug comment and code
```sh
;**********************DEBUG INPUT L00P*******************************;
CODE  CODE  CODE  CODE  CODE  CODE  CODE  CODE  CODE  CODE  CODE
;**********************DEBUG INPUT L00P*******************************;
```
to remove loop input.

Combination of input numbers to test:

1,5,9 |
3,5,6 |
1,9,5 |
2,8,3 |
5,9,1 |
3,7,2 |
5,1,9 |
5,3,8 |
9,1,5 |
3,1,2 |
9,5,1 |
7,6,2 |

Test any other combination.

### Flowchart.
A true or false flow-chart to compare the statement “***a*** is less than ***b***".
Follow through when you run the code and debug to see where the jumps are being made when the statement returns a true or a false result.
**This is not a flow of what is happening in program; this is the flow of the comparison statement.**

Black text in the flowchart are the label names.


![flowchart](https://gitlab.com/freakydinosaur/little-computer-3/raw/master/smallest_and_biggest-decision_tree_method/true_false_flowchart_3_inputs.png)
#### Labels.
There are labels to show parts of the code to show where where they are when you run the debug in the simulator.

Labels are case-senstive and are required for certain part of the code to work to make jumps in branches in the statements or are used to access ASCII in memory
```sh
INPUT                       ; Label
MeSSagE .STRINGZ "Hello"    ;
```

Labels that are commented with
```sh
; Label x
```
can be removed they are not required for the code to work.

#### Registers Assignments.
| Register | Description |
| ------ | ------ |
| R0 | Read/Write |
| R1 | Temp1 |
| R2 | Temp2 |
| R3 | Condition Result |
| R4 | Store value of input 1 |
| R5 | Store value of input 2 |
| R6 | Store value of input 3 |
| R7 | Reserved for program |
